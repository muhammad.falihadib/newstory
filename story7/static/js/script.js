$( document ).ready( function() {
    $( "#accordion" ).accordion({
        header: "h2",
        collapsible: true,
        active: false,
    });
    $("#down1").click(function(){
        $(".line1").next().insertBefore(".line1");
    });
    $("#up1").click(function(){
        $(".line1").prev().insertAfter(".line1");
    });
    $("#down2").click(function(){
        $(".line2").next().insertBefore(".line2");
    });
    $("#up2").click(function(){
        $(".line2").prev().insertAfter(".line2");
    });
    $("#down3").click(function(){
        $(".line3").next().insertBefore(".line3");
    });
    $("#up3").click(function(){
        $(".line3").prev().insertAfter(".line3");
    });
    $("#down4").click(function(){
        $(".line4").next().insertBefore(".line4");
    });
    $("#up4").click(function(){
        $(".line4").prev().insertAfter(".line4");
    });
});