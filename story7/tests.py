from django.test import TestCase, Client

# Create your tests here.
class testReport(TestCase):
    def test_url_index(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_template_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'story7/index.html')