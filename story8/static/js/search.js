$( document ).ready(function() {
	var default_keyword = "Favorite Book";
	var url_target = '/data?q=' + default_keyword;
	search(url_target);
})


$( "#keyword" ).keyup(function() {
	var isi_keyword = $("#keyword").val();
	var url_target = '/data?q=' + isi_keyword;
	search(url_target);
})

function search(url_target){
	$.ajax({
		url: url_target,
		success: function(hasil) {
			console.log(hasil.items);
			var obj_hasil = $("#hasil");
			obj_hasil.empty();

			for (i = 0; i < hasil.items.length; i++) {
				var tmp_title = hasil.items[i].volumeInfo.title;
				var tmp_authors = hasil.items[i].volumeInfo.authors;
				var tmp_publisher = hasil.items[i].volumeInfo.publisher;
				var tmp_categories = hasil.items[i].volumeInfo.categories;
				var tmp_thumbnails = hasil.items[i].volumeInfo.imageLinks.thumbnail;
				console.log(tmp_title);
				obj_hasil.append('<tr><td rowspan="4"><img src=' + tmp_thumbnails + '></td><td> Judul Buku </td><td>' + tmp_title + '</td></tr><tr><td>Pengarang</td><td>' + tmp_authors +'</td></tr><tr><td>Penerbit</td><td>' + tmp_publisher + '</td></tr><tr><td>Kategori</td><td>' + tmp_categories + '</td></tr>');
			}
		}
	})
}