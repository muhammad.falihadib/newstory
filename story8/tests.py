from django.test import TestCase, Client

# Create your tests here.
class testStory8(TestCase):
    def test_url(self):
        response = Client().get('/books/')
        self.assertEqual(response.status_code, 200)

    def test_template_form(self):
        response = Client().get('/books/')
        self.assertTemplateUsed(response, 'story8/index.html')