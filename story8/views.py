from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.
def index(request):
    return render(request, 'story8/index.html')

def search_data(request):
	arg = request.GET['q']
	url_target = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
	r = requests.get(url_target)

	data = json.loads(r.content)
	return JsonResponse(data, safe=False)